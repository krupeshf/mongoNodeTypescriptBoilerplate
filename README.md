# MongoDB Node Typescript Boilerplate

## Use different version of node for debugging
1. brew install nvm
1. Add to `~/.bash_profile`
    1. `export NVM_DIR="$HOME/.nvm"`
    1. `. "/usr/local/opt/nvm/nvm.sh"`
1. `source ~/.bash_profile`
1. `nvm install 9`
1. `nvm use 9`
1. Restart VSCode
