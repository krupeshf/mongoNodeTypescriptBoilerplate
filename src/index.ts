import * as mongodb from "mongodb";
import * as assert from "assert";

type MyDocDefinition = {
    a: number;
};

const conf = {
    url: "mongodb://localhost:27017", // Connection URL
    dbName: "myproject", // Database Name
    collection: "documents"
};

(
    async () => {
        // Use connect method to connect to the server
        const client = await mongodb.MongoClient.connect(conf.url);
        const db = client.db(conf.dbName);

        await resetCollection(db);

        const myDocs: MyDocDefinition[] = [{ a: 1 }, { a: 2 }, { a: 3 }, { a: 1 }];
        await insertDocuments(db, myDocs);

        const foundDocs: MyDocDefinition[] = await findDocuments(db, { a: 1 });
        console.log("Docs found", foundDocs);

        client.close();
    }
)();

async function insertDocuments(db: mongodb.Db, myDocs: MyDocDefinition[]): Promise<mongodb.InsertWriteOpResult> {
    const collection = db.collection(conf.collection);
    const insertManyResult = await collection.insertMany(myDocs);
    assert.equal(myDocs.length, insertManyResult.result.n);
    assert.equal(myDocs.length, insertManyResult.ops.length);
    console.log(`Inserted ${myDocs.length} documents into the collection`);
    return insertManyResult;
}

async function findDocuments(db: mongodb.Db, myDoc: MyDocDefinition): Promise<MyDocDefinition[]> {
    const collection = db.collection(conf.collection);
    return collection.find<MyDocDefinition>(myDoc).toArray();
}

async function resetCollection(db: mongodb.Db) {
    db.collection(conf.collection).drop();
}

async function updateDocument(db: mongodb.Db): Promise<mongodb.UpdateWriteOpResult> {
    const updatedDocument = await db.collection(conf.collection).updateOne({ a: 1 }, { $set: { b: 1 } });
    assert.equal(1, updatedDocument.result.n);
    return updatedDocument;
}
